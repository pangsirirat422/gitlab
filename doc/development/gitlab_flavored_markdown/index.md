---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Markdown developer documentation **(FREE)**

This page contains the MVC for the developer documentation for GitLab Flavored Markdown.
For the user documentation about Markdown in GitLab, refer to
[GitLab Flavored Markdown](../../user/markdown.md).

## GitLab Flavored Markdown specification guide

The [specification guide](specification_guide/index.md) includes:

- [Terms and definitions](specification_guide/index.md#terms-and-definitions).
- [Parsing and rendering](specification_guide/index.md#parsing-and-rendering).
- [Goals](specification_guide/index.md#goals).
- [Implementation](specification_guide/index.md#implementation) of the spec.
